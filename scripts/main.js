const MAPTYPE = {
    land: "land",
    water: "water",
};

const MapPool = [
    {
        id: "great_plains",
        name: "Great Plains",
        image: "images/maps/great_plains.png",
    },
    {
        id: "gran_chaco",
        name: "Gran Chaco",
        image: "images/maps/gran_chaco.png",
    },
    {
        id: "dakota",
        name: "Dakota",
        image: "images/maps/dakota.png",
    },
    {
        id: "lithuania",
        name: "Lithuania",
        image: "images/maps/lithuania.png",
    },
    {
        id: "mongolia",
        name: "Mongolia",
        image: "images/maps/mongolia.png",
    },
    {
        id: "pampas_sierras",
        name: "Pampas Sierras",
        image: "images/maps/pampas_sierras.png",
    },
    {
        id: "saxony",
        name: "Saxony",
        image: "images/maps/saxony.png",
    },
    {
        id: "yukon",
        name: "Yukon",
        image: "images/maps/yukon.png",
    },
    {
        id: "pomerania",
        name: "Pomerania",
        image: "images/maps/pomerania.png",
    },
    {
        id: "siberia",
        name: "Siberia",
        image: "images/maps/siberia.png",
    },
    {
        id: "panama",
        name: "Panama",
        image: "images/maps/panama.png",
    },
    {
        id: "texas",
        name: "Texas",
        image: "images/maps/texas.png",
    },
];

const CIV = {
    spanish: "Spanish",
    british: "British",
    french: "French",
    dutch: "Dutch",
    portuguese: "Portuguese",
    russians: "Russians",
    germans: "Germans",
    ottomans: "Ottomans",
    aztecs: "Aztecs",
    haudenosaunee: "Haudenosaunee",
    lakota: "Lakota",
    chinese: "Chinese",
    japanese: "Japanese",
    indians: "Indians",
    swedish: "Swedish",
    incans: "Incans",
    americans: "United States",
    mexicans: "Mexicans",
    hausa: "Hausa",
    ethiopians: "Ethiopians",
    italians: "Italians",
    maltese: "Maltese",
};

const CivPool = [
    {
        name: "Spanish",
        image: "images/flags/spanish.png",
    },
    {
        name: "British",
        image: "images/flags/british.png",
    },
    {
        name: "French",
        image: "images/flags/french.png",
    },
    {
        name: "Dutch",
        image: "images/flags/dutch.png",
    },
    {
        name: "Portuguese",
        image: "images/flags/portuguese.png",
    },
    {
        name: "Russians",
        image: "images/flags/russian.png",
    },
    {
        name: "Germans",
        image: "images/flags/german.png",
    },
    {
        name: "Ottomans",
        image: "images/flags/ottoman.png",
    },
    {
        name: "Aztecs",
        image: "images/flags/aztec.png",
    },
    {
        name: "Haudenosaunee",
        image: "images/flags/haudenosaunee.png",
    },
    {
        name: "Lakota",
        image: "images/flags/sioux.png",
    },
    {
        name: "Chinese",
        image: "images/flags/chinese.png",
    },
    {
        name: "Japanese",
        image: "images/flags/japanese.png",
    },
    {
        name: "Indians",
        image: "images/flags/indian.png",
    },
    {
        name: "Swedish",
        image: "images/flags/swedish.png",
    },
    {
        name: "Incans",
        image: "images/flags/incan.png",
    },
    {
        name: "United States",
        image: "images/flags/americans.png",
    },
    {
        name: "Mexicans",
        image: "images/flags/mexicans.png",
    },
    {
        name: "Hausa",
        image: "images/flags/hausa.png",
    },
    {
        name: "Ethiopians",
        image: "images/flags/ethiopians.png",
    },
    {
        name: "Italians",
        image: "images/flags/italians.png",
    },
    {
        name: "Maltese",
        image: "images/flags/maltese.png",
    },
];

// TEAM ONE AND THREE ARE DONE!

const Players = [
    {
        name: "Ezad",
        team: 1,
        civs: [CIV.portuguese, CIV.indians, CIV.japanese, CIV.germans, CIV.russians],
        mapBans: ["gran_chaco", "yukon"],
    },
    {
        name: "JulianK",
        team: 1,
        civs: [CIV.ottomans, CIV.ethiopians, CIV.swedish, CIV.french, CIV.aztecs],
        mapBans: ["pomerania", "panama"],
    },
    {
        name: "Frontline",
        team: 2,
        civs: [CIV.indians, CIV.french, CIV.swedish, CIV.americans, CIV.british],
        mapBans: ["siberia", "panama"],
    },
    {
        name: "Optimus",
        team: 2,
        civs: [CIV.portuguese, CIV.japanese, CIV.russians, CIV.aztecs, CIV.ethiopians],
        mapBans: ["saxony", "lithuania"],
    },
    {
        name: "Aykin",
        team: 3,
        civs: [CIV.dutch, CIV.french, CIV.portuguese, CIV.swedish, CIV.ottomans],
        mapBans: ["great_plains", "dakota"],
    },
    {
        name: "ChefSudiste",
        team: 3,
        civs: [CIV.maltese, CIV.ethiopians, CIV.russians, CIV.indians, CIV.spanish],
        mapBans: ["texas", "pomerania"],
    },
    {
        name: "Ungurs",
        team: 4,
        civs: [CIV.lakota, CIV.russians, CIV.ottomans, CIV.germans, CIV.chinese],
        mapBans: ["panama", "pomerania"],
    },
    {
        name: "Minimoult",
        team: 4,
        civs: [CIV.french, CIV.dutch, CIV.maltese, CIV.mexicans, CIV.americans],
        mapBans: ["gran_chaco", "pampas_sierras"],
    },
    {
        name: "King of Osmane",
        team: 5,
        civs: [CIV.ottomans, CIV.dutch, CIV.french, CIV.lakota, CIV.aztecs],
        mapBans: ["lithuania", "pampas_sierras"],
    },
    {
        name: "Kaister",
        team: 5,
        civs: [CIV.americans, CIV.germans, CIV.russians, CIV.spanish, CIV.portuguese],
        mapBans: ["mongolia", "yukon"],
    },
];

function getCivImage(playerCiv) {
    for (const civ of CivPool) {
        if (civ.name === playerCiv) {
            return civ.image;
        }
    }
}
function teamIdToLetter(id) {
    return String.fromCharCode(64 + id);
}

function shuffle(array) {
    let index = array.length;
    let temp = -1;
    let randomIndex = -1;
    while (index !== 0) {
        randomIndex = Math.floor(Math.random() * index);
        index -= 1;
        temp = array[index];
        array[index] = array[randomIndex];
        array[randomIndex] = temp;
    }
    return array;
}
