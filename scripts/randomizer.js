let matchToReveal = 0;

console.log(Players);

//get selection options
const LeftSelectElement = document.querySelector("#team_left");
const RightSelectElement = document.querySelector("#team_right");

LeftSelectElement.addEventListener("change", updateTeamSelection);
RightSelectElement.addEventListener("change", updateTeamSelection);

function updateTeamSelection() {
    const leftTeam = parseInt(LeftSelectElement.value);
    const rightTeam = parseInt(RightSelectElement.value);
    resetFlags();

    if (leftTeam == rightTeam || leftTeam == 0 || rightTeam == 0) {
        RightSelectElement.value = 0;
        return;
    }
    randomizeMatches();
}

// when document is ready, randomize matches
document.addEventListener("DOMContentLoaded", randomizeMatches);

function randomizeMatches() {
    const leftTeam = parseInt(LeftSelectElement.value);
    const rightTeam = parseInt(RightSelectElement.value);

    const leftTeamPlayers = Players.filter((player) => player.team === leftTeam);
    const rightTeamPlayers = Players.filter((player) => player.team === rightTeam);

    const leftP1 = leftTeamPlayers[0];
    const leftP2 = leftTeamPlayers[1];
    const rightP1 = rightTeamPlayers[0];
    const rightP2 = rightTeamPlayers[1];

    document.querySelectorAll(".player .name")[0].innerText = leftP1.name;
    document.querySelectorAll(".player .name")[1].innerText = leftP2.name;
    document.querySelectorAll(".player .name")[2].innerText = rightP1.name;
    document.querySelectorAll(".player .name")[3].innerText = rightP2.name;

    const leftCivOrder = shuffle([...leftP1.civs, ...leftP2.civs]);
    const rightCivOrder = shuffle([...rightP1.civs, ...rightP2.civs]);
    // get first 3 civs for each player
    const leftP1Civs = leftCivOrder.slice(0, 3);
    const leftP2Civs = leftCivOrder.slice(3, 6);
    const rightP1Civs = rightCivOrder.slice(0, 3);
    const rightP2Civs = rightCivOrder.slice(3, 6);

    const playerCivs = [leftP1Civs, leftP2Civs, rightP1Civs, rightP2Civs];
    const mapBans = [];
    mapBans.push(...leftP1.mapBans);
    mapBans.push(...leftP2.mapBans);
    mapBans.push(...rightP1.mapBans);
    mapBans.push(...rightP2.mapBans);

    const mapOrder = shuffle(MapPool);

    console.log("MapBans", mapBans);
    console.log(
        "MapPool",
        mapOrder.map((map) => map.id)
    );
    const matchMapPool = mapOrder.filter((map) => {
        for (ban in mapBans) {
            if (mapBans[ban].includes(map.id)) {
                return false;
            }
        }
        return true;
    });
    console.log(
        "Filtered MapPool",
        matchMapPool.map((map) => map.id)
    );
    const button = document.querySelector("#reveal");
    button.setAttribute("disabled", "disabled");
    setTimeout(() => {
        const playersElements = document.querySelectorAll(".player");
        const mapsElements = document.querySelectorAll(".map");
        playersElements.forEach((player, index) => {
            for (let game = 0; game < 3; game++) {
                player.querySelectorAll(".actual")[game].src = getCivImage(playerCivs[index][game]);
            }
        });
        mapsElements.forEach((map, index) => {
            map.querySelector(".actual").src = matchMapPool[index].image;
            map.querySelector(".name").innerText = matchMapPool[index].name;
        });
        document.querySelectorAll(".map .actual");
        button.removeAttribute("disabled");
    }, 1000);
    console.log(
        `%cTeam ${teamIdToLetter(leftTeam)} vs Team ${teamIdToLetter(rightTeam)}`,
        "background-color: black; color: white; font-size: 20px; padding: 5px;"
    );
    for (let game = 0; game < 3; game++) {
        console.log(
            `Game ${game + 1} - ${leftP1Civs[game]} & ${leftP2Civs[game]} vs ${rightP1Civs[game]} & ${rightP2Civs[game]} on ${
                matchMapPool[game].id
            }`
        );
    }
}

function updateRevealButton() {
    const button = document.querySelector("#reveal");
    if (matchToReveal === 3) {
        button.setAttribute("disabled", "disabled");
    } else {
        setTimeout(() => {
            button.removeAttribute("disabled");
        }, 1000);
    }
    button.textContent = matchToReveal === 3 ? "All revealed" : "Reveal Game " + (matchToReveal + 1);
}
updateRevealButton();

document.querySelector("#reveal").addEventListener("click", () => {
    document.querySelectorAll(".team").forEach((team) => {
        team.querySelectorAll(".player").forEach((player) => {
            player.querySelectorAll(".civilization")[matchToReveal].querySelector(".revealer").classList.add("reveal");
        });
    });
    const mapElement = document.querySelectorAll(".maps .map")[matchToReveal];
    mapElement.querySelector(".revealer").classList.add("reveal");
    mapElement.querySelector(".name").classList.add("reveal");
    matchToReveal += 1;
    updateRevealButton();
});
document.querySelector("#reset").addEventListener("click", () => {
    resetFlags();
});

function resetFlags() {
    for (let i = 0; i < 3; i++) {
        document.querySelectorAll(".team").forEach((team) => {
            team.querySelectorAll(".player").forEach((player) => {
                player.querySelectorAll(".civilization")[i].querySelector(".revealer").classList.remove("reveal");
            });
        });
        document.querySelectorAll(".maps .map")[i].querySelector(".revealer").classList.remove("reveal");
        document.querySelectorAll(".maps .map")[i].querySelector(".name").classList.remove("reveal");
    }
    matchToReveal = 0;
    updateRevealButton();
    setTimeout(() => {
        randomizeMatches();
    }, 500);
}
