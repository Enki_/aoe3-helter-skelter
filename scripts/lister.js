const playersElement = document.querySelector("#app .players");
const displayToggle = document.querySelector("#display_toggle");
let teamDisplayMode = true;
displayToggle.addEventListener("click", () => {
    teamDisplayMode = !teamDisplayMode;
    displayToggle.textContent = teamDisplayMode ? "Columns" : "Rows";
    playersElement.classList.toggle("team-display");
});

const sortedPlayers = Players.sort((a, b) => {
    return a.team - b.team;
});
allBans = [];
for (let i = 1; i < 6; i++) {
    const teamPlayers = sortedPlayers.filter((player) => player.team === i);
    let civs = teamPlayers
        .map((player) => player.civs)
        .flat()
        .sort((a, b) => {
            return a.localeCompare(b);
        });
    let mapBans = teamPlayers
        .map((player) => player.mapBans)
        .flat()
        .sort((a, b) => a.localeCompare(b));

    const teamBans = mapBans.map((ban) => {
        return MapPool.filter((map) => map.id === ban)[0].name;
    });
    allBans.push(...teamBans);
    playersElement.innerHTML += `
    <div class="player">
        <h2 class="name">${teamPlayers[0].name} & ${teamPlayers[1].name}
            <span class="team">(Team ${teamIdToLetter(teamPlayers[0].team)})</span>
            <span class="bans">Map Bans: ${teamBans
                .map((ban) => {
                    return `<span class="ban">${ban}</span>`;
                })
                .join(", ")}.</span>
        </h2>
        <section class="civs">
        ${civs
            .map(
                (civ) => `
            <picture class="civilization">
                <img class="actual" src="${getCivImage(civ)}" alt="" />
            </picture>
        `
            )
            .join("")}
        </section>
    </div>
`;
}
const countBans = {};
for (const ban in allBans) {
    countBans[allBans[ban]] = (countBans[allBans[ban]] || 0) + 1;
}

const banList = Object.keys(countBans)
    .map((map) => {
        return { map, count: countBans[map] };
    })
    .sort((a, b) => b.count - a.count);

const countCivs = {};
for (const civ in CivPool) {
    countCivs[CivPool[civ].name] = 0;
}
for (const player in Players) {
    for (const civ in Players[player].civs) {
        countCivs[Players[player].civs[civ]] += 1;
    }
}
const civList = Object.keys(countCivs)
    .map((civ) => {
        return { civ, count: countCivs[civ] };
    })
    .sort((a, b) => b.count - a.count);

console.log(`Map Ban Frequency`, banList);
console.log(`Civ Pick Frequency`, civList);
